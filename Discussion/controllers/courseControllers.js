const Course = require('../models/Course')

const addCourse = async (req, res) => {
    // Destructure data from req object
    const { name, description, price } = req.body
    
    // Confirm data
    if (!name || !description || !price) {
        return res.status(400).json({ message: 'All fields are required' })
    }

    // Check for duplicate
    const duplicate = await Course.findOne({name})
    if (duplicate) {
        return res.status(400).json({ message: 'Course already exists' })
    }

    // Create course object
    const courseObject = {
        name,
        description,
        price
    }

    // Create and store new course
    const newCourse = await Course.create(courseObject)

    if (newCourse) {
        res.status(201).json({ message: `New course ${name} created`})
    } else {
        res.status(400).json({ message: 'Invalid course data received' })
    }
}

// Retrieve all courses
const getAllCourses = async (req, res) => {
    const courses = await Course.find()

    // Check if there are courses
    if (!courses.length) {
        return res.status(404).json({ message: "No courses found" })
    }

    res.status(200).json(courses)
}

// Retrieve all active courses
const getAllActiveCourses = async (req, res) => {
    const activeCourses = await Course.find({ isActive: true })

    // Check if there are active courses
    if (!activeCourses.length) {
        return res.status(404).json({ message: "There are no active courses "})
    }

    res.status(200).json(activeCourses)
}

// Retrieve specific course
const getCourse = async (req, res) => {
    // Extract id from req params
    const { courseId } = req.params

    // Check if id exists
    if (!courseId) {
        return res.status(404).json({ message: "Course Id is required" })
    }

    // Query for specific course using id
    const course = await Course.findById(courseId)

    // Check if course exists
    if (!course) {
        return res.status(404).json({ message: "Course does not exist" })
    }

    return res.status(200).json(course)
}

// Archive a course
const archiveCourse = async (req, res) => {
    // destructure courseId from req params
    const { courseId } = req.params
    
    // Check if courseId exists
    if (!courseId) {
        return res.status(404).json({ message: "Course Id is required" })
    }

    // destructure data from req body
    const { isActive } = req.body
    
    // Check if course exists
    const course = await Course.findById(courseId)

    if (!course) {
        return res.status(404).json({ message: "Course does not exist" })
    }

    course.isActive = isActive

    // Save new course status
    const archivedCourse = await course.save()

    res.status(200).json({ message: "Course has been archived" })
}

module.exports = {
    addCourse,
    getAllCourses,
    getAllActiveCourses,
    getCourse,
    archiveCourse
}