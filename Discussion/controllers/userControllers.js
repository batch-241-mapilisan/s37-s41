const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const checkEmailExists = async (req, res) => {
    // Deconstruct data from req object
    const { email } = req.body

    // Check for duplicate
    const duplicate = User.findOne({email})
    if (duplicate) {
        return res.status(400).json({ message: "Email is already taken" })
    }

    res.status(200).json({ message: "Email is available for registration" })
}

const registerUser = async (req, res) => {
    // Deconstruct data from req object
    const { firstName, lastName, email, password, mobileNo, isAdmin } = req.body

    // Confirm data
    if (!firstName || !lastName || !email || !password || !mobileNo) {
        return res.status(400).json({ message: "All fields are required "})
    }

    // Check for duplicate
    const duplicate = await User.findOne({ email })
    if (duplicate) {
        return res.status(409).json({ message: 'Email is already taken. Please use another email' })
    }

    // Hash password
    const hashedPassword = await bcrypt.hash(password, 10)
    let userObject;
    if (isAdmin) {
        userObject = {
            firstName,
            lastName,
            email,
            password: hashedPassword,
            mobileNo,
            isAdmin
        }
    } else {
        userObject = {
            firstName,
            lastName,
            email,
            password: hashedPassword,
            mobileNo
        }
    }

    // Create and store new user
    const newUser = await User.create(userObject)

    if (newUser) {
        res.status(201).json({ message: `New user ${email} created` })
    } else {
        res.status(400).json({ message: 'Invalid user data received' })
    }
}

const loginUser = async (req, res) => {
    // Deconstruct data from req object
    const { email, password } = req.body

    // Verify data
    if (!email || !password) {
        return res.status(400).json({ message: 'All fields are required' })
    }

    const foundUser = await User.findOne({email})

    if (!foundUser) {
        return res.status(401).json({ message: 'User is unauthorized' })
    }

    // Check if password matches with hashed password from database
    const match = await bcrypt.compare(password, foundUser.password)

    if (!match) {
        return res.status(401).json({ message: 'Invalid credentials' })
    }

    // Access token generated with node terminal => require('crypto').randomBytes(64).toString('hex')
    const ACCESS_TOKEN_SECRET = 'a31786d16a9cce03cf1c89f8d4630e91ab9f20abe8bcd940dc2a61d1aec6616442074c7ce8152fbd9d508f0438faab75388a72a8c2444659ffdc7b2b45575e0f'

    const accessToken = jwt.sign(
        {
            "UserInfo": {
                "id": foundUser._id,
                "email": foundUser.email,
                "isAdmin": foundUser.isAdmin
            }
        },
        ACCESS_TOKEN_SECRET,
        { expiresIn: '15m' }
    )

    res.json(accessToken)
}

const getProfile = async (req, res) => {
    // Deconstruct data from req object
    const { id } = req.body

    // Verify data
    if (!id) {
        return res.status(400).json({ message: 'User ID is required' })
    }

    // This query excludes password field from getting sent back
    // const foundUser = await User.findById(id).select('-password')

    const foundUser = await User.findById(id)

    if (!foundUser) {
        return res.status(404).json({ message: 'User does not exist' })
    }

    foundUser.password = ''

    res.status(200).json(foundUser)
}

const verify = (req, res, next) => {
    // Access authorization from header
    const authHeader = req.headers.authorization || req.headers.Authorization

    // Check if auth header exists
    if (!authHeader.startsWith('Bearer ')) {
        return res.status(401).json({ message: 'Unauthorized' })
    }

    // Get token from Auth header
    const token = authHeader.split(' ')[1]

    // Compare token with ACESS_TOKEN_SECRET
    const ACCESS_TOKEN_SECRET = 'a31786d16a9cce03cf1c89f8d4630e91ab9f20abe8bcd940dc2a61d1aec6616442074c7ce8152fbd9d508f0438faab75388a72a8c2444659ffdc7b2b45575e0f'

    jwt.verify(
        token,
        ACCESS_TOKEN_SECRET,
        (error, data) => {
            if (error) {
                return res.status(403).json({ message: 'Forbidden' })
            }
            req.email = data.UserInfo.email
            req.isAdmin = data.UserInfo.isAdmin
            next()
        }
    )
}

const verifyIfAdmin = (req, res, next) => {
    // Access authorization from header
    const authHeader = req.headers.authorization || req.headers.Authorization

    // Check if auth header exists
    if (!authHeader.startsWith('Bearer ')) {
        return res.status(401).json({ message: 'Unauthorized' })
    }

    // Get token from Auth header
    const token = authHeader.split(' ')[1]

    // Compare token with ACESS_TOKEN_SECRET
    const ACCESS_TOKEN_SECRET = 'a31786d16a9cce03cf1c89f8d4630e91ab9f20abe8bcd940dc2a61d1aec6616442074c7ce8152fbd9d508f0438faab75388a72a8c2444659ffdc7b2b45575e0f'

    jwt.verify(
        token,
        ACCESS_TOKEN_SECRET,
        (error, data) => {
            if (error || !data.UserInfo.isAdmin) {
                return res.status(403).json({ message: 'Forbidden' })
            }
            req.email = data.UserInfo.email
            req.isAdmin = data.UserInfo.isAdmin
            next()
        }
    )
}

const enroll = async (req, res) => {
    const authHeader = req.headers.authorization || req.headers.Authorization

    // Check if auth header exists
    if (!authHeader.startsWith('Bearer ')) {
        return res.status(401).json({ message: 'Unauthorized' })
    }

    // Get token from auth header
    const token = authHeader.split(' ')[1]

    // Compare token with ACCESS_TOKEN_SECRET
    const ACCESS_TOKEN_SECRET = 'a31786d16a9cce03cf1c89f8d4630e91ab9f20abe8bcd940dc2a61d1aec6616442074c7ce8152fbd9d508f0438faab75388a72a8c2444659ffdc7b2b45575e0f'

    // Extract userId from token
    let userId;
    
    jwt.verify(
        token,
        ACCESS_TOKEN_SECRET,
        (error, decoded) => {
            if (error) {
                return res.status(403).json({ message: 'Forbidden' })
            }
            userId = decoded.UserInfo.id
        }
    )

    // Extract data from req body
    const { courseId } = req.body

    // Check if course exists
    const course = await Course.findById(courseId)

    if (!course) {
        return res.status(404).json({ message: "Course does not exist" })
    }

    const user = await User.findById(userId)

    user.enrollments = [...user.enrollments, {courseId}]

    const updatedUser = await user.save()

    course.enrollees = [...course.enrollees, {userId}]

    const updatedCourse = await course.save()

    res.status(200).json({ message: `User ${updatedUser.firstName} has successfully enrolled on course ${updatedCourse.name}` })
}

module.exports = {
    checkEmailExists,
    registerUser,
    loginUser,
    getProfile,
    verify,
    verifyIfAdmin,
    enroll
}