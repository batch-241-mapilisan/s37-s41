const express = require('express')
const { addCourse, getAllCourses, getAllActiveCourses, getCourse, archiveCourse } = require('../controllers/courseControllers')
const { verifyIfAdmin, verify } = require('../controllers/userControllers')
const router = express.Router()

router.use(verify)
router.get('/all', getAllCourses)
router.get('/active', getAllActiveCourses)
router.get('/:courseId', getCourse)
router.patch('/:courseId/archive', archiveCourse)
router.use(verifyIfAdmin)
router.post('/', addCourse)

module.exports = router