const express = require('express')
const router = express.Router()
const { registerUser, checkEmailExists, loginUser, getProfile, verify, enroll } = require('../controllers/userControllers')

router.post('/checkEmail', checkEmailExists)
router.post('/register', registerUser)
router.post('/login', loginUser)

router.use(verify)
router.post('/details', getProfile)
router.post('/enroll', enroll)

module.exports = router